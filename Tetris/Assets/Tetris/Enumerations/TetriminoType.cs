﻿namespace Tetris
{
	/// <summary>
	/// テトリミノタイプの列挙型
	/// </summary>
	public enum TetriminoType
	{
		I,
		O,
		S,
		Z,
		J,
		L,
		T
	}
}