﻿namespace Tetris
{
	/// <summary>
	/// テトリミノ回転角度の列挙型
	/// </summary>
	public enum RotationAngle
	{
		/// <summary> 0° </summary>
		Degress0,
		/// <summary> 90° </summary>
		Degress90,
		/// <summary> 180° </summary>
		Degress180,
		/// <summary> 270° </summary>
		Degress270
	}
}
