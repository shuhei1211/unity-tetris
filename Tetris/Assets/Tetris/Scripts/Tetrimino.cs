﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Tetris
{
	public class Tetrimino : ITetrimino
	{
		/// <summary>
		/// コンストラクタ
		/// </summary>
		public Tetrimino ()
		{
			// テトリミノのタイプを取得します
			TetriminoType tetriminoType = (TetriminoType)UnityEngine.Random.Range (0, Enum.GetNames (typeof(TetriminoType)).Length);
		}

		public void MoveRight ()
		{
		}

		public void MoveLeft ()
		{
		}

		public void RotateRight ()
		{
		}

		public void RotateLeft ()
		{
		}
	}

}