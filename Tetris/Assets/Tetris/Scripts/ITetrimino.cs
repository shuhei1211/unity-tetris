﻿namespace Tetris
{
	/// <summary>
	/// テトリミノ共通のインターフェース
	/// </summary>
	public interface ITetrimino
	{
		/// <summary> 右移動 </summary>
		void MoveRight ();

		/// <summary> 左移動 </summary>
		void MoveLeft ();

		/// <summary> 右回転 </summary>
		void RotateRight ();

		/// <summary> 左回転 </summary>
		void RotateLeft ();
	}
}
