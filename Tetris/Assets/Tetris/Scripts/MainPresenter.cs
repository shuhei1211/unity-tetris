﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Tetris
{
	public class MainPresenter : MonoBehaviour
	{
		[SerializeField] Button leftButton;
		[SerializeField] Button rightButton;
		[SerializeField] Button rotateRigthButton;
		[SerializeField] Button rotateLeftButton;
		[SerializeField] Text debugTetris;

		List<List<int>> list = new List<List<int>> ();

		// Use this for initialization
		void Start ()
		{
			for (int i = 0; i < 10; i++) {
				List<int> _list = new List<int> ();
				for (int j = 0; j < 20; j++) {
					_list.Add (0);
				}
				list.Add (_list);
			}

			var a = list.SelectMany (x => x);

			foreach (var item in a) {
				Debug.Log (item.ToString ());
			}

			debugTetris.text = "0000000000000\n0000000000";
		}

		// Update is called once per frame
		void Update ()
		{

		}
	}
}
